package demo;

import java.awt.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import java.util.*;

public class formstack {

	@Test
	public void Login() throws InterruptedException 
{
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.formstack.com/ ");
		driver.manage().window().maximize();
	WebElement login=driver.findElement(By.xpath("(//div//ul//a[@href='https://www.formstack.com/admin'])[2]"));	
	login.click();
	WebElement email=driver.findElement(By.xpath("//*[@id='email']"));	
	email.sendKeys("Naveenragultce@gmail.com");
	WebElement password=driver.findElement(By.xpath("//*[@id='password']"));
	password.sendKeys("Nady09nady09.");
	WebElement lsubmit=driver.findElement(By.xpath("//*[@id='submit']"));	
	lsubmit.click();
	Thread.sleep(2000);
	JavascriptExecutor js = (JavascriptExecutor) driver;
	js.executeScript("window.scrollBy(0,500)");
	
	WebElement createtemp=driver.findElement(By.xpath("(//*[@class='fs-btn2 fs-btn2--style_create fs-btn2--size_large'])[2]"));	
	createtemp.click();
	Thread.sleep(1000);
	WebElement form=driver.findElement(By.xpath("//*[@class='FT_b8709 FT_ec648']"));	
	form.click();
	WebElement next=driver.findElement(By.xpath("//*[@class=' phx-Button phx-Button--medium phx-Button--link phx-ActionButton']"));	
	next.click();
	
	WebDriverWait wait=new WebDriverWait(driver, 10);
	
	String firstName = generateRandomName(5);
	WebElement formname = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@type='text' and @name='formName']")));
	formname.sendKeys(firstName);
	
	
			WebElement formurl=driver.findElement(By.xpath("//*[@type='text' and @name='formURL']"));	
	formurl.sendKeys(firstName);
	
	WebElement startwithtemp=driver.findElement(By.xpath("//*[@class=' phx-Button phx-Button--medium phx-Button--link phx-ActionButton']"));	
	startwithtemp.click();
	
	java.util.List<WebElement> list=driver.findElements(By.xpath("//*[@class='FT_e5e71']"));
System.out.println("Size is "+list.size());

//System.out.println("name of elements are" +list.get(i).getAttribute() );	

for (WebElement webElement : list) {
    String name = webElement.getText();
    System.out.println(name);
}

WebElement nextstep=driver.findElement(By.xpath("//*[@class='phx-Button-content' and text()='Next Step']"));
nextstep.click();

WebElement theme=driver.findElement(By.xpath("//*[@style='fill: rgb(55, 64, 70); stroke: rgb(172, 181, 191);']"));
theme.click();

js.executeScript("window.scrollBy(0,500)");
//*[@class=' phx-Button phx-Button--medium phx-Button--link phx-ActionButton']

WebElement finish=driver.findElement(By.xpath("//*[@class=' phx-Button phx-Button--medium phx-Button--link phx-ActionButton']"));
finish.click();
	}
	
	public String generateRandomName(int length) {
	    char[] chars ="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" .toCharArray();
	    StringBuilder sb = new StringBuilder();
	    Random random = new Random();
	    for (int i = 0; i < length; i++) {
	        char c = chars[random.nextInt(chars.length)];
	        sb.append(c);
	    }
	    String randomString = sb.toString();
	    return randomString;
	}
}
